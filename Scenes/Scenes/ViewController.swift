//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

class ViewController: UIViewController {

  @IBOutlet var renderView: RRGLRenderView!
  fileprivate var sceneIndex: Int = 0
  fileprivate var sceneNames: [String] = ["street_girl", "eiffeltower"]



  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    // Preparing Scene Layout and try-on
    let sceneLayout = RRSceneLayout.init()
    
    let avatarFilePath = Bundle.main.path(forResource: "MKP_9166", ofType: "avatar")!
    let avatar = RRAvatar.init(fromFile: avatarFilePath)!
    sceneLayout!.avatarRenderable = RRAvatarRenderable.init(avatar: avatar)
    
    let garmentFilePath1 = Bundle.main.path(forResource: "shirt", ofType: "garment")!
    let garment1 = RRGarment.init(fromFile: garmentFilePath1)!
    let garmentRenderable1 = RRGarmentRenderable.init(garment: garment1)!
    
    let garmentFilePath2 = Bundle.main.path(forResource: "pants2", ofType: "garment")!
    let garment2 = RRGarment.init(fromFile: garmentFilePath2)!
    let garmentRenderable2 = RRGarmentRenderable.init(garment: garment2)!
    
    let garmentFilePath3 = Bundle.main.path(forResource: "jacket", ofType: "garment")!
    let garment3 = RRGarment.init(fromFile: garmentFilePath3)!
    let garmentRenderable3 = RRGarmentRenderable.init(garment: garment3)!
    
    sceneLayout!.garmentRenderables = [garmentRenderable1, garmentRenderable2, garmentRenderable3]
    self.renderView!.layout = sceneLayout
    
    self.switchScene()
  }

  /**
   Switching between provided scenes including loading a scene from file
   */
  @IBAction func switchScene() {
    self.sceneIndex = (self.sceneIndex + 1) % self.sceneNames.count
  
    // load the scene from the .arscene file
    let scenePath = Bundle.main.path(forResource: self.sceneNames[self.sceneIndex], ofType: "arscene")!
    
    let binaryFormatProvider = RRBinarySceneGraphFormat.init()
    let serializer = RRSceneGraphSerializer.init(formatProvider: binaryFormatProvider!)
    
    self.renderView.removeAllRenderables();
  
    try! serializer?.deserialize(fromPath: scenePath, renderView: self.renderView)
  }


}

