# Scenes Sample Application
This sample shows how to display 2D try-ons in a 3D scene where camera orientation changes based on device rotation.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore iOS SDK:
* Loading and rendering predefined scenes from file using the `RRSceneLayout class`
* Switching between different predefined scenes
* Rendering a 2D try-on with 2 different garments

## Dependencies
To build the sample project, you'll need to place the `PictofitCore.xcframework` in the parent directory. 

## Screenshots
![](Screenshots/Scene1.jpg) ![](Screenshots/Scene2.jpg)
