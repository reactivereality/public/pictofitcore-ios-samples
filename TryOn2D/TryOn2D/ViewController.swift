//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

/**
 This sample shows how to render a 2D try-on and how to switch garments using tap gestures
 */
class ViewController: UIViewController {
  @IBOutlet var renderView: RRGLRenderView!
  
  var upperGarments: [RRGarmentRenderable] = []
  var lowerGarments: [RRGarmentRenderable] = []
  
  var upperGarmentIndex: Int = 0
  var lowerGarmentIndex: Int = 0

  var tryOnLayout: RRUserPhotoLayout = RRUserPhotoLayout.init()!


  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    // Load avatar
    let avatarPath = Bundle.main.path(forResource: "martina", ofType: "avatar")
    let avatar = RRAvatar.init(fromFile: avatarPath!)
    let avatarRenderable = RRAvatarRenderable(avatar: avatar!)
    avatarRenderable.backgroundHidden = true
    self.tryOnLayout.avatarRenderable = avatarRenderable
    
    // Load garments
    self.upperGarments.append(self.loadGarmentRenderableFromResource(resourceName: "blouse", type: "garment"))
    self.upperGarments.append(self.loadGarmentRenderableFromResource(resourceName: "sweater", type: "garment"))

    self.lowerGarments.append(self.loadGarmentRenderableFromResource(resourceName: "pants", type: "garment"))
    self.lowerGarments.append(self.loadGarmentRenderableFromResource(resourceName: "pants2", type: "garment"))
    
    self.updateGarmentRenderables()
    
    self.renderView.layout = self.tryOnLayout
  }
  
  fileprivate func updateGarmentRenderables()
  {
    self.tryOnLayout.garmentRenderables = [self.lowerGarments[self.lowerGarmentIndex], self.upperGarments[self.upperGarmentIndex]]
  }
  
  @IBAction func viewWasTapped(sender: UITapGestureRecognizer!)
  {
    let touchLocation = sender.location(in: self.renderView)
    if touchLocation.y < 0.5 * self.renderView.bounds.size.height {
      self.upperGarmentIndex = (self.upperGarmentIndex + 1) % self.upperGarments.count
    }
    else {
      self.lowerGarmentIndex = (self.lowerGarmentIndex + 1) % self.lowerGarments.count
    }
    
    self.updateGarmentRenderables()
  }
  
  fileprivate func loadGarmentRenderableFromResource(resourceName: String!, type: String!) -> RRGarmentRenderable
  {
    let garmentPath = Bundle.main.path(forResource: resourceName, ofType: type)
    let garment = RRGarment.init(fromFile: garmentPath!)
    let garmentRenderable = RRGarmentRenderable.init(garment: garment!)
    return garmentRenderable!
  }


}

