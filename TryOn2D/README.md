# TryOn2D Sample Application
This sample shows how to easily mix and match 2D garments on a 2D avatar. Providing a large set of garments, this can be seen as a virtual dressing room.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore iOS SDK:
* Loading an `RRAvatar` from file and creating an `RRAvatarRenderable` from that
* Loading an `RRGarment` from file and creating an `RRGarmentRenderable` from that
* Using the `RRUserPhotoLayout` to render a 2D try on using an avatar and multiple garments

## Dependencies
To build the sample project, you'll need to place the `PictofitCore.xcframework` in the parent directory. 

## Screenshots
![](Screenshots/TryOn1.jpg) ![](Screenshots/TryOn2.jpg)
