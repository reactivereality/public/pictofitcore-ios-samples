//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

class ViewController: UIViewController {

  @IBOutlet weak var renderView: RRGLRenderView!
  var tryonSlot : RRTryOnSlot! = RRTryOnSlot.init()
  var shadowSlot : RRQuadSlot! = RRQuadSlot.init()
  var accessorySlot1 : RRGarmentSlot! = RRGarmentSlot.init()
  var accessorySlot2 : RRGarmentSlot! = RRGarmentSlot.init()

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    let slotLayout = RRSlotLayout()
    
    let avatarPath = Bundle.main.path(forResource: "MKP_9063", ofType: "avatar")
    let avatar = RRAvatar.init(fromFile: avatarPath!)
    let avatarRenderable = RRAvatarRenderable(avatar: avatar!)
    avatarRenderable.backgroundHidden = true
    
    let garmentPath1 = Bundle.main.path(forResource: "pants", ofType: "garment")
    let garment1 = RRGarment.init(fromFile: garmentPath1!)
    let garmentRenderable1 = RRGarmentRenderable(garment: garment1!)!

    let garmentPath2 = Bundle.main.path(forResource: "tshirt", ofType: "garment")
    let garment2 = RRGarment.init(fromFile: garmentPath2!)
    let garmentRenderable2 = RRGarmentRenderable(garment: garment2!)!
    
    tryonSlot.frame = self.renderView.bounds
    tryonSlot.avatarRenderable = avatarRenderable
    tryonSlot.garmentRenderables = [garmentRenderable1, garmentRenderable2]
    tryonSlot.zoomToAvatar = false
    tryonSlot.scalingMode = .scaleToHeight
    
    let shadowImage = UIImage.init(named: "shadow.png")
    let shadowRenderable = RRQuadRenderable.init(image: RRImage.init(image: shadowImage!))
    shadowSlot.quadRenderable = shadowRenderable
    shadowSlot.scalingMode = .scaleToHeight
    shadowSlot.frame = self.renderView.bounds
    
    let accessoryPath1 = Bundle.main.path(forResource: "bag", ofType: "garment")
    let accessory1 = RRGarment.init(fromFile: accessoryPath1!)
    accessorySlot1.garmentRenderable = RRGarmentRenderable.init(garment: accessory1!)
    accessorySlot1.scalingMode = .aspectFit
    
    let accessoryPath2 = Bundle.main.path(forResource: "shoes", ofType: "garment")
    let accessory2 = RRGarment.init(fromFile: accessoryPath2!)
    accessorySlot2.garmentRenderable = RRGarmentRenderable.init(garment: accessory2!)
    accessorySlot2.scalingMode = .aspectFit
    
    slotLayout?.slots = [ shadowSlot, tryonSlot, accessorySlot1, accessorySlot2]
    renderView.layout = slotLayout
    
  }
  
  override func viewDidLayoutSubviews() {
    let tryOnRect = CGRect.init(x: 0.0, y: 0.0, width: 0.5 * self.renderView.bounds.size.width, height: self.renderView.bounds.size.height)

    tryonSlot.frame = tryOnRect
    shadowSlot.frame = tryOnRect
    
    accessorySlot1.frame = CGRect.init(x: 0.5 * self.renderView.bounds.size.width, y: 0.0, width: 0.5 * self.renderView.bounds.size.width, height: 0.5 * self.renderView.bounds.size.height)
    
    accessorySlot2.frame = CGRect.init(x: 0.5 * self.renderView.bounds.size.width, y: 0.5 * self.renderView.bounds.size.height, width: 0.5 * self.renderView.bounds.size.width, height: 0.5 * self.renderView.bounds.size.height)
  }
}

