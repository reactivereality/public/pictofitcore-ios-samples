# OrbitViewer Sample Application
This sample shows how to interactively show/rotate 3D objects using the PictofitCore SDK. This functionality can become very handy when showing 3D models in an environment where the augmented reality approach is inappropriate like for instance very small rooms.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore iOS SDK:
* Asynchronous loading of a 3D garment model and creating of the according renderable
* Using the `RROrbitViewerLayout` class to render the object and enable interactive rotation of the 3D object using touch gestures

## Dependencies
To build the sample project, you'll need to place the `PictofitCore.xcframework` in the parent directory. 

## Screenshots
![](Screenshots/OrbitViewer1.jpg) ![](Screenshots/OrbitViewer2.jpg)
