//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//


import UIKit
import PictofitCore

/**
 This ViewController shows how to asynchronously load large 3d objects using an activity indicator and a small preview version of the large 3d model.
 */
class ViewController: UIViewController {
  @IBOutlet var renderView: RRGLRenderView!
  @IBOutlet var activityIndicatorOverlay: UIView!
  fileprivate var orbitLayout: RROrbitViewerLayout = RROrbitViewerLayout.init()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.renderView.layout = self.orbitLayout
    
    self.loadRenderableAsync()
  }
  
  /**
  Loading the 3d model alynchronously
  */
  fileprivate func loadRenderableAsync() {

    DispatchQueue.global(qos: .userInitiated).async {
      
      let parentRenderable = RRRenderable()
    
      let previewPath = Bundle.main.path(forResource: "accessory_bag_black_preview", ofType: "tm3d")!
      let previewModel = RRTexturedMesh3DObject.load(fromFile: previewPath)!
      
      
      // Loading the preview in the current async queue
      let previewRenderable = previewModel.createRenderable()
      
      DispatchQueue.main.sync {
        // Updating the preview renderable in the main queue
        try! parentRenderable.addChild(previewRenderable)
        self.orbitLayout.renderable = parentRenderable
      }

      // Loading the full size model in the current async queue
      let garmentPath = Bundle.main.path(forResource: "accessory_bag_black", ofType: "gm3d")!
      let garment = RRGarment3D.init()
      try! garment.load(fromFile: garmentPath, largeObjectDataProvider: nil)


      // Loading the preview in the current async queue
      let fullSizeRenderable = RRGarment3DRenderable.init(garment3D: garment)

      DispatchQueue.main.sync {
        // Updating the full size renderable in the main queue and hiding the activity indicator
        parentRenderable.removeAllChildren()
        try! parentRenderable.addChild(fullSizeRenderable)
        self.activityIndicatorOverlay.isHidden = true
      }
    }
  }

}

