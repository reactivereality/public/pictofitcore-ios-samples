# PBR based Mannequin Sample Application
This sample shows how to create mannequin avatars and how to load and edit materials.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore iOS SDK:
* Creating mannequin avatars using the `RRPersonalizedMannequinCreator class`
* Using the `RROrbitViewerLayout class` to render a renderable within a scene and focus to the renderable
* Loading materials from file
* Editing PBR materials interactively using the `RRMeshPBRMaterialEditor class`
* Setting up a large object data provider for 3D body models using the `RRLargeObjectDataProviderDirectory class`

## Dependencies
To build the sample project, you'll need to place the `PictofitCore.xcframework` in the parent directory. 

## Screenshots
![](Screenshots/Mannequin1.jpg) ![](Screenshots/Mannequin2.jpg)
