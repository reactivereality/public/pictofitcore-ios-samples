//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

class ViewController: UIViewController {
  @IBOutlet var renderView: RRGLRenderView!
  @IBOutlet var slider: UISlider!
  var materialEditor: RRMeshPBRMaterialEditor!
  var orbitLayout: RROrbitViewerLayout = RROrbitViewerLayout()
  var avatarRenderable: RRAvatar3DRenderable?
  var genderIsMale: Bool = false

  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    // Loading the material and initializing the editor to use the loaded material
    let materialPath = Bundle.main.path(forResource: "mannequin", ofType: "materialstate")
    let material = RRMaterial.create(fromFilePath: materialPath!)
    self.materialEditor = RRMeshPBRMaterialEditor(material: material!)

    // Creating an empty parent renderable to be able to place the skybox and the avatar as children
    let parent = RRRenderable()
    
    // Creating the skybox
    let skyBox = RRSkyBoxRenderable.init()
    skyBox.toneMappingEnabled = true
    try! parent.addChild(skyBox)
    
    // Attaching renderable/layout to renderView
    self.orbitLayout.renderable = parent
    self.renderView.layout = self.orbitLayout
    
    // Initializing slider to the current metallicFactor material property value
    self.slider.value = Float(self.materialEditor.metallicFactor)

    // Place the avatar renderable in the scene
    self.placeAvatarRenderable()
  }
  
  func placeAvatarRenderable() {
    // Remove currently displayed avatar from scene
    self.avatarRenderable?.removeFromParent()

    // Create mannequin avatar:
    let visualRepresentationFilename = self.genderIsMale ? "stylized_manneqeuin_male" : "stylized_manneqeuin_female"
    let bodyModelID = self.genderIsMale ? "DAZ_Genesis8Male_default" : "DAZ_Genesis8Female_default"
    let visualRepresentationPath = Bundle.main.path(forResource: visualRepresentationFilename, ofType: "tm3d")
    let visualRepresentation = RRTexturedMesh3DObject.load(fromFile: visualRepresentationPath!)!
    visualRepresentation.texture = RRImage(image: UIImage.init(named: "mannequin-texture.png")!)
    
    let bodyModel = RRBodyModel3D.create(fromBodyModelID: bodyModelID, largeObjectDataProvider: ViewController.largeObjectDataProvider!)
    let bodyModelState = RRBodyModel3DState(bodyModel: bodyModel!)
    let mannequin = try! RRPersonalizedMannequinCreator.createMannequinAvatar(with: bodyModelState, visualRepresentation: visualRepresentation)

    // Creating a renderable for the mannequin avatar
    self.avatarRenderable = RRAvatar3DRenderable(avatar3D: mannequin)
    
    // Retrieving the visual representation renderable which actually represents the rendered geometry
    let visualRepresentationRenderable = self.avatarRenderable!.visualRepresentationRenderable
    try! self.orbitLayout.renderable?.addChild(self.avatarRenderable!)
    
    // Let the camera focus to the avatar renderable
    self.orbitLayout.targetBoundingBox = self.avatarRenderable!.getBoundingBoxLocal(withHiddenRenderablesIncluded: false)

    // Make sure the visual representation renderable is an RRMeshRenderable since we can just modify materials for this renderable class
    guard let meshRenderable = visualRepresentationRenderable as? RRMeshRenderable else {
      return
    }
    
    // Attach the material from the material editor to the renderable
    meshRenderable.material = self.materialEditor.material
    meshRenderable.setReceiveShadingFactor(1.0, recursive: true)
  }
  
  @IBAction func switchGender(sender: UIButton?) {
    self.genderIsMale = !self.genderIsMale
    self.placeAvatarRenderable()
  }

    
  @IBAction func sliderValueChanged(sender: UISlider?) {
    self.materialEditor.metallicFactor = CGFloat(sender!.value)
  }
  
  static var largeObjectDataProvider: RRLargeObjectDataProviderDirectory? = {
    guard let configPath = Bundle.main.resourceURL?
            .appendingPathComponent("LargeObjectDataFiles/large_object_data_provider_config.json").path else { return nil }
    guard let directoryPath = Bundle.main.resourceURL?.appendingPathComponent("LargeObjectDataFiles").path else { return nil }
    guard let config = RRLargeObjectDataProviderDirectoryConfig.load(fromConfigFile: configPath) else { return nil }
    return RRLargeObjectDataProviderDirectory(config: config, dataDirectoryPath: directoryPath)
  }()
}

