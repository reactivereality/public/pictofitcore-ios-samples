# ARView Sample Application
This sample shows how to use PictofitCore to load 3d objects from files and place them in an augmented reality scene. It also shows how to interactively select 3d objects inside the augmented reality scene.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore iOS SDK:
* Asynchronous loading of different 3D objects from file
* Creating Renderables to visualize 3D object instances
* Using the `RRARView` class to use the PictofitCore rendering engine in combination with an ARKit session
* Using `RRPlaneCollider` class to add detected ground planes from ARKit to the hit testing pipeline of the `RRGLRenderView` class
* Interactive selection of 3D renderables in an AR scene using the `RRCarouselRenderable `class
* Creating a custom design for the carousel circle that is shown at the bottom of the carousel
* Placing 3D renderables in the AR scene
* Select and translate/rotate 3D renderables in the AR scene


## Dependencies
To build the sample project, you'll need to place the `PictofitCore.xcframework` in the parent directory. 

## Screenshots
![](Screenshots/Carousel.jpg) ![](Screenshots/Avatar.jpg)
