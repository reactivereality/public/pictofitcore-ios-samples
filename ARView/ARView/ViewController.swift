//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

/**
 This ViewController manages the basic user interaction in this AR demo application.
 It creates and maintains RRRenderables attached with RRMeshCollider instances for ground planes detected by ARKit. These renderables are added to the RRGLRenderView instance of the displayed RRARView.
 By adding this renderables it gets intersections from the renderView based on touch positions that tell us if a detected plane or a virtual object was hit by this touch.
 Based on this intersections it can decide whether to manipulate existing objects or to place an RRCarouselRenderable for selecting/placing a new object.
 */
class ViewController: UIViewController {
  @IBOutlet var arView: RRARView!
  @IBOutlet var deleteButton: UIButton!

  var planeRenderables: [ARPlaneAnchor: RRRenderable] = [:]
  var placedRenderables: [RRRenderable] = []
  var carouselDataSource: ARObjectsDataSource = ARObjectsDataSource()
  var dragAndDropHandler: RenderableDragAndDropHandler?

  
  var tapGestureRecognizer: UITapGestureRecognizer!
  var rotationGestureRecognizer: UIRotationGestureRecognizer!
  var draggingGestureRecognizer: UIPanGestureRecognizer!
  
  /**
  This function gets the first intersection of a hit test based on a touch position in the view
  */
  fileprivate func getFirstIntersection(touchPosition: CGPoint!) -> RRIntersection? {
    let ray = self.arView.renderView.ray(fromViewPosition: touchPosition)
    let intersections = self.arView.renderView.getIntersections(ray)
    
    return intersections.first
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.arView.sessionDelegate = self
    self.view.isMultipleTouchEnabled = true
    
    // Create and register necessary gesture recognizers:
    self.tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.viewTapped(sender:)))
    self.view.addGestureRecognizer(self.tapGestureRecognizer)
    
    self.draggingGestureRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(self.draggingGestureRecognized(sender:)))
    self.draggingGestureRecognizer.maximumNumberOfTouches = 1
    self.view.addGestureRecognizer(self.draggingGestureRecognizer)
    
    self.rotationGestureRecognizer = UIRotationGestureRecognizer.init(target: self, action: #selector(self.rotationGestureRecognized(sender:)))
    self.view.addGestureRecognizer(self.rotationGestureRecognizer)
    self.rotationGestureRecognizer.isEnabled = false
  }
  
  /**
  This function places a carousel renderable at the position of a given intersection
  */
  fileprivate func placeCarousel(intersection: RRIntersection!) {
    let transformation = RRTransformation.init()
    transformation.translation = intersection.intersectionPoint
    
    let carouselRenderable = RRCarouselRenderable.init()
    carouselRenderable.transformation = transformation
    
    
    carouselRenderable.minimumItemScaleAngularDistance = 45.0
    carouselRenderable.minimumItemScale = 0.5
    carouselRenderable.angularItemsDistance = 42.0
    carouselRenderable.visibleAngularRange = 240.0
    carouselRenderable.carouselRadius = 0.22
    
    carouselRenderable.dataSource = self.carouselDataSource
    carouselRenderable.delegate = self
    
    try! self.arView.renderView.add(carouselRenderable)
    
    self.tapGestureRecognizer.isEnabled = false
    self.draggingGestureRecognizer.isEnabled = false
    
    self.createCustomCarouselCircle(carouselRenderable: carouselRenderable)

    print("Placed Carousel Renderable")
  }
  
  /**
  This function creates a custom circle on the bottom of a RRCarouselRenderable instance.
  */
  fileprivate func createCustomCarouselCircle(carouselRenderable: RRCarouselRenderable) {

    let innerCircleRadius = 1.3 * carouselRenderable.carouselRadius
    let outerCircleRadius = 1.5 * carouselRenderable.carouselRadius
    let lineWidth : CGFloat = 0.005
    let subdivisions : UInt = 100

    // Hide the carousel's default circle:
    carouselRenderable.carouselCircleIsHidden = true

    // Create a custom circle renderable using a RRPathRenderer instance
    let circleRenderable = RRRenderable()
    let circlePathRenderer = RRPathRenderer()
    
    circlePathRenderer.addFilledEllipse(withCenter: CGPoint.zero, extendX: innerCircleRadius, extendY: innerCircleRadius, color: UIColor.lightGray.withAlphaComponent(0.5), subdivisions: subdivisions)
    circlePathRenderer.addEllipse(withCenter: CGPoint.zero, extendX: outerCircleRadius, extendY: outerCircleRadius, lineWidth: lineWidth, color: UIColor.yellow, subdivisions: subdivisions)

    circleRenderable.attach(circlePathRenderer)
    
    // RRPathRenderer renders in the XY plane and we want to render the circle in the XZ plane:
    let transformation = RRTransformation()
    transformation.rotationAngles = simd_float3(-90.0, 0.0, 0.0)
    circleRenderable.transformation = transformation
    
    try! carouselRenderable.addChild(circleRenderable)
  }
  
  /**
  This function performs the selection of a renderable that was previously placed in the scene. The visualization and drag/drop functionality that is activated by this function is implemented in RenderableDragAndDropHandler
  */
  fileprivate func selectPlacedRenderable(renderable: RRRenderable!) {
    self.deselectCurrentlySelectedRenderable()
    self.dragAndDropHandler = RenderableDragAndDropHandler.init(renderable: renderable)
    self.dragAndDropHandler!.selectRenderable()
    self.rotationGestureRecognizer.isEnabled = true
    self.deleteButton.isHidden = false
  }
  
  /**
  Deselect the currently selected placed renderable
  */
  fileprivate func deselectCurrentlySelectedRenderable() {
    self.dragAndDropHandler?.dropRenderable()
    self.dragAndDropHandler = nil
    self.rotationGestureRecognizer.isEnabled = false
    
    self.deleteButton.isHidden = true
  }
  
  /**
   Remove the currently selected object from the scene
   */
  @IBAction func deleteButtonClicked() {
    self.placedRenderables.removeAll { (renderable) -> Bool in
      guard let result = self.dragAndDropHandler?.renderable.isEqual(to: renderable) else {
        return false
      }
      return result
    }
    
    self.dragAndDropHandler?.renderable.removeFromParent()
    self.dragAndDropHandler = nil
    self.rotationGestureRecognizer.isEnabled = false
    
    self.deleteButton.isHidden = true
  }
  
  /**
  This function is invoked by the tap gesture recognizer. Depending on the current selection state and the hit renderable this function
   decides whether to select/deselect a placed renderable or to place a carousel to place another renderable
  */
  @objc func viewTapped(sender: UITapGestureRecognizer) {

    let location = sender.location(in: self.arView.renderView)
    
    if self.dragAndDropHandler != nil {
      self.deselectCurrentlySelectedRenderable()
      return
    }
    
    guard let intersection = getFirstIntersection(touchPosition: location) else {
      return
    }
    
    var tappedGroundPlane: Bool = false

    for renderable in self.planeRenderables {
      if renderable.value.isEqual(to: intersection.renderable) {
        tappedGroundPlane = true
      }
    }
    
    
    if tappedGroundPlane == true {
      self.placeCarousel(intersection: intersection)
    }
    else {
      for renderable in self.placedRenderables {
        if intersection.renderable.hasParent(renderable, recursive: true) {
          self.selectPlacedRenderable(renderable: renderable)
        }
      }
    }
  }
  
  /**
  This function is invoked by the rotation gesture recognizer. If self.dragAndDropHandler is set, this forwards the gesture recognizer events to this instance.
  */
  @objc func rotationGestureRecognized(sender: UIRotationGestureRecognizer) {
    let angle = sender.rotation
    switch sender.state {
    case .began:
      self.dragAndDropHandler?.rotationGestureBegan(rotationAngle: angle)
    case .changed:
      self.dragAndDropHandler?.rotationGestureMoved(rotationAngle: angle)
    case .ended, .cancelled:
      self.dragAndDropHandler?.rotationGestureEnded(rotationAngle: angle)
    default:
      break
    }
    
  }
  
  /**
  This function is invoked by the pan gesture recognizer. If a placed renderable was hit by the touch, the events are forwarded to self.dragAndDropHandler, which then handles the dragging of the renderable.
  */
  @objc func draggingGestureRecognized(sender: UIPanGestureRecognizer) {
    let touchPosition = sender.location(in: self.arView)
    let ray = self.arView.renderView.ray(fromViewPosition: touchPosition)
    
    var intersection = nil as RRIntersection?
    
    if sender.state == .began {
      intersection = self.arView.renderView.getIntersections(ray).first
      
      if intersection == nil { // No renderable was hit with the first position of the gesture, so ignore the event
        return
      }
      
      // If no renderable is currently selected, select the hit one if one was hit
      if self.dragAndDropHandler == nil && sender.state == .began {
        for renderable in self.placedRenderables {
          if intersection!.renderable.hasParent(renderable, recursive: true) {
            self.selectPlacedRenderable(renderable: renderable)
          }
        }
      }
      
      if self.dragAndDropHandler == nil { // No renderable is selected, so ignore the event
        return
      }
      
      if intersection!.renderable.hasParent(self.dragAndDropHandler!.renderable, recursive: true) == false { // Not the currently selected renderable was hit by the touch, so ignore the event
        return
      }
    }
    
    if self.dragAndDropHandler?.getIsDragging() == false && sender.state != .began { // self.dragAndDropHandler is not in a dragging gesture right now even though the gesture did not just begin, so ignore the event
      return
    }
    

    // Forward events to self.dragAndDropHandler if not nil
    switch sender.state {
    case .began:
      self.dragAndDropHandler?.draggingGestureBegan(ray: ray, intersectionPosition: intersection!.intersectionPoint)
    case .changed:
      self.dragAndDropHandler?.draggingGestureMoved(ray: ray)
    case .ended, .cancelled:
      self.dragAndDropHandler?.draggingGestureEnded(ray: ray)
    default:
      break
    }
  }
  
  /**
  This function updates an existing mesh collider according to an ARPlaneAnchor detected by ARKit
  */
  fileprivate func updatePlaneRenderableForAnchor(planeAnchor: ARPlaneAnchor) {
    let renderable = planeRenderables[planeAnchor]!
    let colliders = renderable.colliders
    for collider in colliders {
      renderable.detachCollider(collider)
    }
    
    let planeMesh = RRMesh3D.init(arPlaneAnchor: planeAnchor)
    let meshCollider = RRMeshCollider.init(mesh3D: planeMesh)
    renderable.attachCollider(meshCollider)
    
    let transformation = RRTransformation.init(transformationMatrix: planeAnchor.transform)
    renderable.transformation = transformation
  }
  
}

/**
 Maintain detected planes and add/remove/update their according colliders/renderables based on ARSessionDelegate events
 */
extension ViewController: ARSessionDelegate {
  
  func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
    for anchor in anchors {
      guard let planeAnchor = anchor as? ARPlaneAnchor else {
        continue
      }
      
      let planeRenderable = RRRenderable.init()
      try! self.arView.renderView.add(planeRenderable)
      
      self.planeRenderables[planeAnchor] = planeRenderable
      
      self.updatePlaneRenderableForAnchor(planeAnchor: planeAnchor)
    }
  }
  
  func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
    for anchor in anchors {
      guard let planeAnchor = anchor as? ARPlaneAnchor else {
        continue
      }
      
      self.updatePlaneRenderableForAnchor(planeAnchor: planeAnchor)
    }
  }
  
  func session(_ session: ARSession, didRemove anchors: [ARAnchor]) {
    for anchor in anchors {
      guard let planeAnchor = anchor as? ARPlaneAnchor else {
        continue
      }
      
      self.planeRenderables[planeAnchor]?.removeFromParent()
      self.planeRenderables.removeValue(forKey: planeAnchor)
    }
  }
}


/**
 Adding new placed objects based on RRCarouselRenderableDelegate events and removing the carousel from the scene
*/
extension ViewController: RRCarouselRenderableDelegate {
  func carousel(_ carousel: RRCarouselRenderable, itemWasSelectedAt index: UInt) {
    print("Item " + index.description + " selected")
    carousel.removeFromParent()
    let renderable = self.carouselDataSource.getFullSizeRenderable(index: index)
    
    let transformation = renderable!.transformation
    transformation.translation = transformation.translation + carousel.transformation.translation
    renderable!.transformation = transformation
    
    try! self.arView.renderView.add(renderable!)
    self.placedRenderables.append(renderable!)
    self.tapGestureRecognizer.isEnabled = true
    self.draggingGestureRecognizer.isEnabled = true
  }
  
  func carouselRegisteredTapOutsideBoundingBox(_ carousel: RRCarouselRenderable) {
    print("Close Carousel")
    carousel.removeFromParent()
    self.tapGestureRecognizer.isEnabled = true
    self.draggingGestureRecognizer.isEnabled = true
  }
}



