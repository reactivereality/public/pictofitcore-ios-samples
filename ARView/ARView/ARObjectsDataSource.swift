//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

/**
This class serves as a datasource for an RRCarouselRenderable and also provides functionality to get a full size renderable for an item selected in the carousel. The full size model is then loaded asynchronously.
 The small carousel preview meshes and their textures are preloaded in the initializer function of this class
*/
class ARObjectsDataSource: NSObject {
  enum ModelType {
    case TexturedMesh3D
    case Avatar3D
    case Garment3D
    case Unknown
  }
  
  fileprivate var previewModelNames: [String] = []
  fileprivate var placedModelNames: [String] = []
  fileprivate var spinningWheelImage: RRImage
  

  
  override init() {
    self.previewModelNames = ["floriane_3_preview.tm3d", "edward_2_preview.tm3d", "accessory_bag_black_preview.tm3d","accessory_bag_blue_preview.tm3d", "accessory_female_shoe_preview.tm3d", "accessory_hat_black_preview.tm3d", "female_blouse_blue_preview.tm3d", "female_dress_blue_preview.tm3d", "female_shirt_blue_preview.tm3d", "male_jeans_blue_preview.tm3d", "male_suit_jacket_blue_preview.tm3d", "accessory_male_boot_preview.tm3d"]
    self.placedModelNames = ["floriane_3_compressed.av3d", "edward_2_compressed.av3d", "accessory_bag_black.gm3d", "accessory_bag_blue.gm3d", "accessory_female_shoe.gm3d", "accessory_hat_black.gm3d", "female_blouse_blue.gm3d", "female_dress_blue.gm3d", "female_shirt_blue.gm3d", "male_jeans_blue.gm3d", "male_suit_jacket_blue.gm3d", "accessory_male_boot.gm3d"]
    
    let spinningWheelImageFileFath = Bundle.main.path(forResource: "SpinningWheel", ofType: "png")!
    self.spinningWheelImage = RRImage.init(image: UIImage.init(contentsOfFile: spinningWheelImageFileFath)!)
  }
  
  /**
  The spinning wheel renderable needs to be transformed from XY plane to XZ plane
  */
  fileprivate func getSpinningWheelRenderableTransformation(size: Float) -> RRTransformation {
    let rotationMatrixTransposed = simd_float4x4.init(columns: (simd.simd_float4.init(Float(1.0), Float(0.0), Float(0.0), Float(0.0)),
                                                        simd.simd_float4.init(Float(0.0), Float(0.0), Float(1.0), Float(0.0)),
                                                        simd.simd_float4.init(Float(0.0), Float(-1.0), Float(0.0), Float(0.0)),
                                                        simd.simd_float4.init(Float(0.0), Float(0.0), Float(0.0), Float(1.0))))
    let rotationMatrix = simd_transpose(rotationMatrixTransposed)
    
    let transformation = RRTransformation.init(transformationMatrix: rotationMatrix)
    transformation.scale = simd_float3.init(repeating: size)
    return transformation
  }
  
  /**
  Evaluate model type from file ending
  */
  fileprivate func getModelTypeForFileName(fileName: String) -> ModelType {
    let fileNameNSString = NSString.init(string: fileName)
    
    let fileEnding = String(fileNameNSString.pathExtension)
    
    var result = ModelType.Unknown
    
    switch fileEnding {
    case "tm3d":
      result = .TexturedMesh3D
    case "av3d":
      result = .Avatar3D
    case "gm3d":
      result = .Garment3D
    default:
      break
    }
   
    return result
  }
  
  fileprivate func loadRenderableSync(fileName: String) -> RRRenderable? {
    let fileNameNSString = NSString(string: fileName)
    let filePath = Bundle.main.path(forResource: fileNameNSString.deletingPathExtension, ofType: fileNameNSString.pathExtension)!
    
    var renderable : RRRenderable? = nil

    let modelType = self.getModelTypeForFileName(fileName: fileName)
    switch modelType {
    case .TexturedMesh3D:
      let texturedMesh = RRTexturedMesh3DObject.load(fromFile: filePath)!
      renderable = texturedMesh.createRenderable()
    case .Avatar3D:
      let avatar = RRAvatar3D.init()
      try! avatar.load(fromFile: filePath, largeObjectDataProvider: nil)
      renderable = RRAvatar3DRenderable.init(avatar3D: avatar)
    case .Garment3D:
      let garment = RRGarment3D.init()
      try! garment.load(fromFile: filePath, largeObjectDataProvider: nil)
      renderable = RRGarment3DRenderable.init(garment3D: garment)
    default:
      break
    }
    
    return renderable
  }
  
  /**
   Calculate the transformation for a preview model
   */
  fileprivate func calculatePreviewModelTransformation(renderable: RRRenderable!) ->RRTransformation {
    let boundingBox = renderable.getBoundingBoxLocal(withHiddenRenderablesIncluded: false)
    var lowerBoundingBoxCenter = boundingBox.center
    lowerBoundingBoxCenter[1] = boundingBox.min[1]
    
    var scale = Float.infinity
    
    let maximumBoundingBoxSize = simd_float3(x: 0.2, y: 0.4, z: 0.2)
    for i in 0...2 {
      let tempScale = maximumBoundingBoxSize[i] / (boundingBox.max[i] - boundingBox.min[i])
      scale = min(scale, tempScale)
    }
    
    let result = RRTransformation()
    result.scale = simd_float3.init(repeating: scale)
    result.translation = -lowerBoundingBoxCenter * scale
    
    return result
  }
  
  /**
  This function returns an asynchronously loaded renderable for a 3d model file that appears as a spinning wheel until the actual model loading has succesfully finished.
  */
  fileprivate func getAsyncLoadedModel(fileName: String, spinningWheelSize: Float, modelTransformation: RRTransformation) -> RRRenderable {
    let parent = RRRenderable.init()
    
    let spinningWheelRenderable = RRSpinningImageRenderable.init(image: self.spinningWheelImage)
    spinningWheelRenderable.transformation = self.getSpinningWheelRenderableTransformation(size: spinningWheelSize)
    spinningWheelRenderable.numberOfDiscretizedRotationSteps = 8
    try! parent.addChild(spinningWheelRenderable)
    
    
    DispatchQueue.global(qos: .userInitiated).async {
      let renderable = self.loadRenderableSync(fileName: fileName)!
      renderable.transformation = modelTransformation
      
      DispatchQueue.main.async {
        spinningWheelRenderable.removeFromParent()
        try! parent.addChild(renderable)
      }
    }
    
    return parent
  }
  
  /**
  This function returns an asynchronously loaded preview renderable for a 3d model file that appears as a spinning wheel until the actual model loading has succesfully finished. The transformation of the preview renderable is calculated based on its boundingbox
  */
  fileprivate func getAsyncLoadedPreviewModel(fileName: String, spinningWheelSize: Float) -> RRRenderable {
    let parent = RRRenderable.init()
    
    let spinningWheelRenderable = RRSpinningImageRenderable.init(image: self.spinningWheelImage)
    spinningWheelRenderable.transformation = self.getSpinningWheelRenderableTransformation(size: spinningWheelSize)
    spinningWheelRenderable.numberOfDiscretizedRotationSteps = 8
    try! parent.addChild(spinningWheelRenderable)
    
    
    DispatchQueue.global(qos: .userInitiated).async {
      let renderable = self.loadRenderableSync(fileName: fileName)!
      renderable.transformation = self.calculatePreviewModelTransformation(renderable: renderable)
      
      DispatchQueue.main.async {
        spinningWheelRenderable.removeFromParent()
        try! parent.addChild(renderable)
      }
    }
    
    return parent
  }
  
  /**
  This function provides the full size renderable according to a selected preview item index of the carosuel data it provides. The returned renderable is a spinning wheel until the full model is succesfully loaded
  */
  func getFullSizeRenderable(index: UInt) -> RRRenderable! {
    let modelIndex = Int(index) % self.placedModelNames.count

//    let transformation = self.modelTransformations[modelIndex].copy() as! RRTransformation
//    transformation.scale = 4.0 * transformation.scale
//    return self.getAsyncLoadedModel(meshFileName: self.meshNames[modelIndex], textureFileName: self.textureNames[modelIndex], spinningWheelSize: 0.3, modelTransformation: transformation)
    
    let transformation = RRTransformation.init()
//    transformation.scale = 0.1 * transformation.scale
    return self.getAsyncLoadedModel(fileName: self.placedModelNames[modelIndex], spinningWheelSize: 0.3, modelTransformation: transformation)
  }
}

/**
ARObjectsDataSource implementation to provide the data for an RRCarouselRenderable instance
*/
extension ARObjectsDataSource: RRCarouselRenderableDataSource {
  func numberOfItems(inCarousel carousel: RRCarouselRenderable) -> UInt {
    return UInt(self.placedModelNames.count)
  }
  
  /**
  This function provides the preview renderable according to a requested preview item index. The returned renderable is a spinning wheel until the preview model is succesfully loaded
  */
  func carousel(_ carousel: RRCarouselRenderable, getItemRenderableAt index: UInt) -> RRRenderable {
    let modelIndex = Int(index) % self.previewModelNames.count
    
//    let transformation = self.modelTransformations[modelIndex]
    
    
    return self.getAsyncLoadedPreviewModel(fileName: self.previewModelNames[modelIndex], spinningWheelSize: 0.075)

//    return self.getAsyncLoadedModel(meshFileName: self.previewMeshNames[modelIndex], textureFileName: self.previewTextureNames[modelIndex], spinningWheelSize: 0.075, modelTransformation: transformation)
  }
}
