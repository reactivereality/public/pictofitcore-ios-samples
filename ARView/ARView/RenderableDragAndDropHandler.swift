//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//


import UIKit
import PictofitCore

/**
This class provides the drag and drop handling for renderables placed in the scene. It uses the RRHoveringRenderable to visualize a selected renderable.
 It implements dragging and rotation based on gesture recognizer events called from the ViewController instance.
*/
class RenderableDragAndDropHandler {
  
  var renderable: RRRenderable
  
  fileprivate var rotationAngleGestureBegan: CGFloat = 0.0
  fileprivate var draggingPlanePositionGestureBegan: simd_float3 = simd_float3(0.0, 0.0, 0.0)
  fileprivate var draggingPlaneY: CGFloat = 0.0
  fileprivate var isDragging: Bool = false

  init(renderable: RRRenderable!) {
    self.renderable = renderable
  }
  
  /**
  This function starts the hovering animation using the RRHoveringRenderable class by inserting an RRHoveringRenderable as parent of the selected renderable if not already present
  */
  func selectRenderable() {
    var hoveringRenderable = self.renderable.parent as? RRHoveringRenderable
    
    if hoveringRenderable == nil {
      hoveringRenderable = RRHoveringRenderable.init(automaticLiftingEnabled: false, autoRemoveOnDroppingFinishedEnabled: true)
      if self.renderable.parent != nil {
        try! self.renderable.parent!.addChild(hoveringRenderable!)
        self.renderable.removeFromParent()
        try! hoveringRenderable!.addChild(self.renderable)
      }
    }
    
    hoveringRenderable!.startHovering()
  }
  
  /**
  This function ends the hovering animation. When the dropping animation is finished, the hovering renderable removes itself automatically from the renderable tree
  */
  func dropRenderable() {
    let hoveringRenderable = self.renderable.parent as? RRHoveringRenderable
    if hoveringRenderable != nil {
      hoveringRenderable!.endHovering()
    }
  }
  
  /**
  This function ends the hovering animation. When the dropping animation is finished, the hovering renderable removes itself automatically from the renderable tree
  */
  func calculateDraggingPlaneIntersection(ray: RRRay!) -> simd_float3 {
    if ray.direction.y == 0.0 {
      return simd_float3(0.0, 0.0, 0.0) // TODO: do something reasonable
    }
    
    let t = (Float(self.draggingPlaneY) - ray.origin.y) / ray.direction.y
    return ray.origin + t * ray.direction
  }
  
  func draggingGestureBegan(ray: RRRay!, intersectionPosition:simd_float3!) {
    self.draggingPlaneY = CGFloat(intersectionPosition.y)
    let transformation = self.renderable.transformation
    let intersectionPosition = self.calculateDraggingPlaneIntersection(ray: ray)
    
    self.draggingPlanePositionGestureBegan = intersectionPosition - transformation.translation
    self.renderable.transformation = transformation
    
    self.isDragging = true
  }
  
  func draggingGestureMoved(ray: RRRay!) {
    let transformation = self.renderable.transformation
    let intersectionPosition = self.calculateDraggingPlaneIntersection(ray: ray)
    transformation.translation = intersectionPosition - self.draggingPlanePositionGestureBegan
    self.renderable.transformation = transformation
  }
  
  func draggingGestureEnded(ray: RRRay!) {
    self.draggingGestureMoved(ray: ray)
    self.isDragging = false
  }
  
  func rotationGestureBegan(rotationAngle: CGFloat!) {
    let transformation = self.renderable.transformation
    let currentRotation = transformation.rotationAngles.y
    self.rotationAngleGestureBegan = rotationAngle + CGFloat(currentRotation / 180.0 * Float.pi)
  }
  
  func rotationGestureMoved(rotationAngle: CGFloat!) {
    let transformation = self.renderable.transformation
    transformation.rotationAngles = simd_float3.init(x: 0.0, y: Float(self.rotationAngleGestureBegan - rotationAngle) * 180.0 / Float.pi, z: 0.0)
    self.renderable.transformation = transformation
  }
  
  func rotationGestureEnded(rotationAngle: CGFloat!) {
    self.rotationGestureMoved(rotationAngle: rotationAngle) // Similar things to do like in moved event
  }
  
  func getIsDragging() -> Bool {
    return self.isDragging
  }
  
}
