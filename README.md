# Readme

Currently, the following samples are included:
* ARView - Loading and interactive selection of 3D renderables in an AR scene
* TryOn2D - Rendering a 2D Try-On of multiple garments
* Scenes - Rendering a 2D Try-On in a 3D scene
* OffscreenRendering - Offscreen render to image functionality
* OrbitViewer - Interactive rotation of a 3D garment
* SlotLayout - 2D renderable layouting using a slot based layout
* PBRBasedMannequin - Creating and editing a mannequin with PBR material


## Dependencies
To build the sample projects, you'll need to run `pod install` for each respective sample app that you want to build. Make sure to open the `.xcworkspace` file created by running `pod install` rather than the `.xcodeproj` file so the PictofitCore dependency can be linked properly. For more infos on how to use cocoa pods visit <https://cocoapods.org>.
