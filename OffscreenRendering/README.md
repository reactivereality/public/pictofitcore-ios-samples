# OffscreenRendering Sample Application
This sample shows how to use the PictofitCore SDK to render arbitrary scenes to images without the necessity of displaying them while rendering. Use this functionality if you want to pre-render images for any purpose.

This sample application includes code snippets showing how to implement the following tasks using PictofitCore iOS SDK:
* Creating a simple 2D try-on scene using the `RRUserPhotoLayout` class
* Rendering the 2D try-on to an image using the offscreen rendering feature of the `RRGLRenderView` class

## Dependencies
To build the sample project, you'll need to place the `PictofitCore.xcframework` in the parent directory. 

## Screenshots
![](Screenshots/Offscreen.jpg)
