//
//  Copyright © 2014-2020 Reactive Reality. All rights reserved.
//

import UIKit
import PictofitCore

class ViewController: UIViewController {

  var renderView : RRGLRenderView!
  var avatarRenderable : RRAvatarRenderable!
  @IBOutlet var imageView : UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.

  }

  @IBAction func renderPressed() {
    //Initialize with forOffscreenRendering init function
    self.renderView = RRGLRenderView.init(forOffscreenRendering: ())

    let renderLayout = RRUserPhotoLayout.init()
    renderLayout?.animationsEnabled = false


    renderLayout?.backgroundColor = UIColor.clear
    let garment1 = RRGarmentRenderable.init(garment: RRGarment.init(fromFile: Bundle.main.path(forResource: "pants", ofType: "garment")!)!)
    let garment2 = RRGarmentRenderable.init(garment: RRGarment.init(fromFile: Bundle.main.path(forResource: "tshirt", ofType: "garment")!)!)

    renderLayout?.garmentRenderables = [garment1!, garment2!]
    //Make sure to keep a strong reference to the AvatarRenderable, because otherwise the "avatarRenderablehasTryonReady" callback won't be fired!
    //Waiting for the "avatarRenderablehasTryonReady" callback is always needed if a tryon is involved. Otherwise the garments can appear not properly fitted to the avatar in the rendering!
    self.avatarRenderable = RRAvatarRenderable.init(avatar: RRAvatar.init(fromFile: Bundle.main.path(forResource: "MKP_9166", ofType: "avatar")!)!)
    self.avatarRenderable.backgroundHidden = true
    self.avatarRenderable.setTryonReadyCallback { [weak self] in
      self?.avatarRenderablehasTryonReady()
    }
    renderLayout?.avatarRenderable = self.avatarRenderable

    self.renderView!.layout = renderLayout
  }
  
  func avatarRenderablehasTryonReady() {
    let screenResolution = UIScreen.main.nativeBounds.size
    let resultImage = self.renderView?.render(toImage: screenResolution)

    self.imageView.image = resultImage?.convertToUIImage()

    //For stopping the RenderView from consuming memory/runtime its reference should be deleted when not needing it currently any more.
    self.renderView = nil
    self.avatarRenderable = nil
  }
}

